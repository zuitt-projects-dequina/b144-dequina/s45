// Import the Fragment component from react
// this will disregard the comma needed in multiple components
// import { Fragment } from 'react'; router will be used instead, hence fragment is not necessary anymore
import { Container } from 'react-bootstrap';
// BrowserRouter is a router implementation that uses the HTML5 history API(pushState, replaceState and the popstate event) to keep your UI in sync with the URL. It is the parent component that is used to store all of the other components
// Router - allows us to build a single-page web application with navigation without the page refreshing as the user navigates. React Router uses component structure to call components, which display the appropriate information
import { BrowserRouter as Router } from 'react-router-dom';
// Switch - It connects multiple networked devices in the network; Switches toggle the state of a single setting on or off
// Route - the conditionally shown component that renders some UI when its path matches the current URL 
import { Route, Switch } from 'react-router-dom';
import { useState, useEffect } from 'react';
import AppNavbar from './components/AppNavBar';
import Courses from './pages/Courses';
import CourseView from './components/CourseView';
import Register from './pages/Register';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/Error';
import './App.css';
import { UserProvider } from './UserContext';
// Import the bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  // State hook for the user state that's defined for a global scope
  // Initialize an object with properties from the localStorage
  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on Logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

    // order of components matter
  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/courses" component={Courses} />
            <Route exact path="/courses/:courseId" component={CourseView} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/logout" component={Logout} />
            <Route exact path="/register" component={Register} />
            <Route component={PageNotFound} />
          </Switch>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
