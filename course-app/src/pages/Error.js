// import { NavLink } from "react-router-dom";
import Banner from '../components/Banner';


export default function PageNotFound() {

    const errorData = {
        title: "Page Not Found",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Go back Home."
    }
    
    // Redirect back to homepage
    return (
        <Banner data={errorData} />
        
        // <div>
        //     <h1>Page Not Found</h1>
        //     <span>Go back to <NavLink exact to='/'>Homepage</NavLink></span>
        // </div>
    )
}
