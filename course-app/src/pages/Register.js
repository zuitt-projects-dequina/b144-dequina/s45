import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect, useHistory, useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
    // Allows to consume the User context object and it's properties to use for user validation
    const { user, setUser } = useContext(UserContext);
    const history = useHistory();
    const { userId } = useParams();

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [age, setAge] = useState('');
    const [gender, setGender] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    // State to determine wether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Check if values are successfully binded
    /*    console.log(firstName);
       console.log(lastName);
       console.log(email);
       console.log(mobileNo);
       console.log(password1);
       console.log(password2); */


    // Function to simulate user registration
    function registerRefresh(e) {
        // Prevents page redirection via from submission
        e.preventDefault();

        // Clear input fields 
        setFirstName('');
        setLastName('');
        setAge('');
        setGender('');
        setEmail('');
        setMobileNo('');
        setPassword1('');
        setPassword2('');

        console.log('New user successfully registered!');
    }



    // an Enroll Function to enroll a user to a specific course
    const registerUser = (userId) => {

        fetch(`http://localhost:4000/api/users/register`, {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                age: age,
                gender: gender,
                email: email,
                mobileNo: mobileNo,
                password: password1
            })
        })
            .then(res => res.json())
            .then(data => {

            })
    }


    // Check email
    const retrieveUserEmail = (emailData) => {
        fetch('http://localhost:4000/api/users/checkemail', {
            method: "POST",
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(email);
                console.log(data);


                if (data === false) {
                    Swal.fire({
                        title: "Successfully registered.",
                        icon: "success",
                        text: "You are successfully registered."
                    })
                    registerUser();
                    history.push("/login");
                }
                else {
                    Swal.fire({
                        title: "Duplicate email found.",
                        icon: "error",
                        text: "Plase provide a different email."
                    })
                }
            })
    }

    // useEffect Hook allows you to perform side effects in your components. Some examples of side effects are: fetching data, directly updating the DOM, and timers. useEffect accepts two arguments
    useEffect(() => {
        // Validation to enable the Submit Button when all fields are populated and both passwords match
        if ((firstName !== '' && lastName !== '' && age !== '' && gender !== "") && (mobileNo.length === 11) && (email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)  ) {
            setIsActive(true)
        }
        else {
            setIsActive(false)
        }
    }, [email, password1, password2]);



    return (
        (user.id !== null) ?
            <Redirect to="/courses" />
            :
            <Form onSubmit={(e) => registerRefresh(e)}>

                <Form.Group className="mb-3" controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter First Name"
                        value={firstName}
                        onChange={e => setFirstName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter Last Name"
                        value={lastName}
                        onChange={e => setLastName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="age">
                    <Form.Label>Age</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter Age"
                        value={age}
                        onChange={e => setAge(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="gender">
                    <Form.Label>Gender</Form.Label>
                    <Form.Control
                        as="select"
                        placeholder="Enter Gender (Male or Female)"
                        value={gender}
                        onChange={e => setGender(e.target.value)}
                        required
                    >

                        <option type="text" value="" disabled>Select a Gender</option>
                        <option type="text" value="male">Male</option>
                        <option type="text" value="female">Female</option>
                        <option type="text" value="other">Other</option>
                    </Form.Control>
                </Form.Group>

                <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="mobileNo">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter Mobile Number"
                        value={mobileNo}
                        onChange={e => setMobileNo(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={password1}
                        onChange={e => setPassword1(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Verify Password"
                        value={password2}
                        onChange={e => setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>

                {/* Conditionally render submit button based on isActive state */}
                {isActive ?
                    <Button variant="primary" type="submit" id="submitBtn" onClick={() => retrieveUserEmail(userId)}>
                        Submit
                    </Button>
                    :
                    <Button variant="danger" type="submit" id="submitBtn" disabled >
                        Submit
                    </Button>
                }


            </Form>
    )
}