// Mock Database
const courseData = [
    {
        id : "wdc001",
        name : "PHP - Laravel",
        description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus cumque ipsa similique excepturi, blanditiis officia eius labore natus ad unde! Recusandae vitae modi aliquam, quidem eos laborum ab deserunt esse!",
        price : 45000,
        onOffer : true
    },
    {
        id : "wdc002",
        name : "Python - Django",
        description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit ipsa fuga maxime, culpa inventore ratione atque, itaque incidunt voluptas praesentium ex natus sint cum harum aut deserunt quod facere voluptatum.",
        price : 50000,
        onOffer : true
    },
    {
        id : "wdc003",
        name : "Java - Sringboot",
        description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati cupiditate provident, a eveniet culpa officiis id quam qui dolore dolores laudantium sit explicabo in debitis! Odit ratione quisquam cumque odio?",
        price : 55000,
        onOffer : true
    },
    {
        id : "wdc004",
        name : "HTML - Introduction",
        description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam reprehenderit inventore possimus unde laboriosam expedita eveniet sit provident quis saepe libero vel delectus alias debitis veritatis, aut animi officiis deleniti!",
        price : 60000,
        onOffer : true
    }
    
];

export default courseData;